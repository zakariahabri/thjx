///
// This file has been generated, if you wish to
// modify it in a permanent way, please refer
// to the script file : gen/generator_cxx.rb
//

#include "prologin.hh"
#include <climits>

///
// Function called at the start of the game. You can use it to initialize your data structures.
//
void game_init()
{
  srand(time(NULL));
}

///
// Function called during the bidding phase. If you never call the ``bid`` function, you will bid zero resources.
//
void bidding_phase()
{
    int intial_bid = 10 + rand() % 16;
    if (resources_owned(me()) > intial_bid)
        bid(intial_bid);
    else
        bid(0);
}

///
// Function called during the outbidding phase.
//
void outbidding_phase()
{
    int maxBid = 0;
    if (element_sold() == ELT_LUMIERE)
        maxBid = resources_owned(me());
    else if (element_sold() == ELT_AIR || element_sold() == ELT_FEU)
        maxBid = 75;
    else if (element_sold() == ELT_TERRE)
        maxBid = 100;
    if (player_bid(opponent()) + outbid_cost() <= maxBid && maxBid <= resources_owned(me()))
        outbid();
}

///
// Function called during the fighting phase. If you never call the ``fight`` function, you will play the first element you own, ordered by F > E > T > A > L.
//
void fighting_phase()
{
    if (nb_element_owned(me(), ELT_LUMIERE) > 1)
        fight(ELT_LUMIERE);
    else if (nb_element_owned(opponent(), ELT_LUMIERE) > 1)
    {
        auto elements = elements_owned(me());
        element best_response = ELT_INVALID;
        int best_gain = INT_MIN;
        for (element elt : elements)
        {
            int gain = resources_gain(elt, ELT_LUMIERE);
            if (gain > best_gain)
            {
                best_gain = gain;
                best_response = elt;
            }
        }
        fight(best_response);
    }
    else
    {
        auto elements = elements_owned(me());
        auto opp_elements = elements_owned(opponent());
        element best_elt = ELT_INVALID;
        int best_gain = INT_MIN;
        for (element elt : elements)
        {
            int worse = INT_MAX;
            for (element opp_elt : opp_elements)
            {
                if (elt != ELT_LUMIERE && opp_elt == ELT_LUMIERE)
                    continue;

                int gain = resources_gain(elt, opp_elt);
                if (gain < best_gain)
                    worse = gain;
            }
            if (worse > best_gain)
            {
                best_gain = worse;
                best_elt = elt;
            }
        }
        if (best_gain >= 0)
            fight(best_elt);
        else if (elements.size() == 1)
            fight(elements.at(0));
        else
            fight(elements.at(rand() % (elements.size() - 1)));
    }
}

///
// Function called at the end of the game. You can use it to free your data structures.
//
void game_end()
{
  // fonction a completer
}


///
// This file has been generated, if you wish to
// modify it in a permanent way, please refer
// to the script file : gen/generator_cxx.rb
//

#ifndef PROLOGIN_HH_
# define PROLOGIN_HH_

# include <vector>

# include <string>

///
// Initial resources of each player at the start of the game.
//
# define INITIAL_RESOURCES         1000

///
// Resources needed to win the game.
//
# define WIN_RESOURCES             20000

///
// The number of elements.
//
# define NB_ELEMENTS               5

///
// Errors returned by the actions
//
typedef enum action_error
{
  OK, /* <- The action was performed successfully. */
  INVALID_ARGUMENT, /* <- Invalid argument received. */
  WRONG_PHASE, /* <- This action cannot be performed during this phase. */
  LACK_ELEMENT, /* <- You do not own this element type. */
  LACK_RESOURCES, /* <- You need more resources to perform this action. */
  ALREADY_CALLED, /* <- You cannot call this action multiple times. */
} action_error;


///
// Type of elements
//
typedef enum element
{
  ELT_FEU, /* <- Feu */
  ELT_EAU, /* <- Eau */
  ELT_TERRE, /* <- Terre */
  ELT_AIR, /* <- Air */
  ELT_LUMIERE, /* <- Lumière */
  ELT_INVALID, /* <- Invalid element */
} element;


///
// Bid resources in the elements auction.
//
extern "C" action_error api_bid(int amount);
static inline action_error bid(int amount)
{
  return api_bid(amount);
}


///
// Outbid the current best bid.
//
extern "C" action_error api_outbid();
static inline action_error outbid()
{
  return api_outbid();
}


///
// Play an element against the other player.
//
extern "C" action_error api_fight(element elt);
static inline action_error fight(element elt)
{
  return api_fight(elt);
}


///
// Returns your player identifier (not always in {0, 1}).
//
extern "C" int api_me();
static inline int me()
{
  return api_me();
}


///
// Returns your opponent identifier (not always in {0, 1}).
//
extern "C" int api_opponent();
static inline int opponent()
{
  return api_opponent();
}


///
// The current score of the specified player.
//
extern "C" int api_score(int player);
static inline int score(int player)
{
  return api_score(player);
}


///
// The set of elements owned by a specific player.
//
extern "C" std::vector<element> api_elements_owned(int player);
static inline std::vector<element> elements_owned(int player)
{
  return api_elements_owned(player);
}


///
// The quantity of an element owned by a specific player.
//
extern "C" int api_nb_element_owned(int player, element elt);
static inline int nb_element_owned(int player, element elt)
{
  return api_nb_element_owned(player, elt);
}


///
// The quantity of all the elements owned by a specific player.
//
extern "C" int api_nb_elements_owned(int player);
static inline int nb_elements_owned(int player)
{
  return api_nb_elements_owned(player);
}


///
// The gain of an element played against another (returns a value from the gain matrix).
//
extern "C" int api_resources_gain(element yours, element theirs);
static inline int resources_gain(element yours, element theirs)
{
  return api_resources_gain(yours, theirs);
}


///
// The amount of resources owned by a player.
//
extern "C" int api_resources_owned(int player);
static inline int resources_owned(int player)
{
  return api_resources_owned(player);
}


///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
extern "C" element api_last_element_played(int player);
static inline element last_element_played(int player)
{
  return api_last_element_played(player);
}


///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
extern "C" element api_element_sold();
static inline element element_sold()
{
  return api_element_sold();
}


///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
extern "C" int api_player_initial_bid(int player);
static inline int player_initial_bid(int player)
{
  return api_player_initial_bid(player);
}


///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
extern "C" int api_player_bid(int player);
static inline int player_bid(int player)
{
  return api_player_bid(player);
}


///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
extern "C" int api_outbid_cost();
static inline int outbid_cost()
{
  return api_outbid_cost();
}


///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
extern "C" int api_bid_difference();
static inline int bid_difference()
{
  return api_bid_difference();
}


///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
extern "C" bool api_auction_canceled();
static inline bool auction_canceled()
{
  return api_auction_canceled();
}


///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
extern "C" int api_auction_winner();
static inline int auction_winner()
{
  return api_auction_winner();
}


///
// Affiche le contenu d'une valeur de type action_error
//
extern "C" void api_afficher_action_error(action_error v);
static inline void afficher_action_error(action_error v)
{
  api_afficher_action_error(v);
}


///
// Affiche le contenu d'une valeur de type element
//
extern "C" void api_afficher_element(element v);
static inline void afficher_element(element v)
{
  api_afficher_element(v);
}



extern "C"
{
///
// Function called at the start of the game. You can use it to initialize your data structures.
//
void game_init();

///
// Function called during the bidding phase. If you never call the ``bid`` function, you will bid zero resources.
//
void bidding_phase();

///
// Function called during the outbidding phase.
//
void outbidding_phase();

///
// Function called during the fighting phase. If you never call the ``fight`` function, you will play the first element you own, ordered by F > E > T > A > L.
//
void fighting_phase();

///
// Function called at the end of the game. You can use it to free your data structures.
//
void game_end();
}
#endif
